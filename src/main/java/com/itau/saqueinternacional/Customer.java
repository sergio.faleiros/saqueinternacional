package com.itau.saqueinternacional;

public abstract class Customer {

	protected String nome;
	protected String usuario;
	protected String senha;
	protected double saldo;
	
	
	
	public Customer(String nome, String usuario, String senha, double saldo) {
		this.nome = nome;
		this.usuario = usuario;
		this.senha = senha;
		this.saldo = saldo;
	}

	public boolean saque(double saque) {
		System.out.println("Saque Mae :");
		if (saldo>=saque) {
			saldo -= saque;
			return true;
		}else {
		return false;
		}
	}

	@Override
	public String toString() {
		return nome + "," + usuario + "," + senha + "," + saldo;
	}
	
	public String getHeader() {
		return " ";
	}
	
}
