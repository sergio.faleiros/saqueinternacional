package com.itau.saqueinternacional;

public class BasicCustomer extends Customer{

	public BasicCustomer(String nome, String usuario, String senha, double saldo) {
		super(nome, usuario, senha, saldo);
	}

	@Override
	public boolean saque(double saque) {

		double saqueTaxa = saque + (saque * 0.025);

		System.out.println("Saque Basic :");

		if (saldo>= saqueTaxa) {
			saldo -= saqueTaxa;
			return true;
		}else {
			return false;
		}
	}	


}
